<?php
/**
 * Classe de utilitários do sistema
 * Aqui você irá encontrar todas as validações, organizações e formatações do sistema
 *
 * @author Luiz Henrique Pereira <luizpereiragit@gmail.com>
 * @package modelo
 * @subpackage util
 * @version 1.0
 */
Final Class Util
{
	
	/**
	 * Método que irá retirar as letras da string
	 *
	 * @access public
	 * @param string $string String para formatação
	 * @return string
	 */
	public static function retirarCaracteres( $string )
	{
		
		//Definindo os arrays de acentuação
		$arrAcentos		= array("Á","É","Í","Ó","Ú","Â","Ê","Î","Ô","Û","Ã","Ñ","Õ","Ä","Ë","Ï","Ö","Ü","À","È","Ì","Ò","Ù","á","é","í","ó","ú","â","ê","î","ô","û","ã","ñ","õ","ä","ë","ï","ö","ü","à","è","ì","ò","ù","A","a","B","b","C","c","D","d","E","e","F","f","G","g","H","h","I","i","J","j","K","k","L","l","M","m","N","n","O","o","P","p","Q","q","R","r","S","s","T","t","U","u","W","w","V","v","X","x","Y","y","Z","z","$"," ");
		$arrSemacento	= array("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","");
		
		//Formatando a string
		$string 		= str_replace( $arrAcentos, $arrSemacento, $string );
		
		//Retornando a string
		return $string;
		
	}
	
	/**
	 * Método de formatação da data.
	 * Existirão vários "meios" para formatação da data passada.
	 * Ex.
	 * 	1. "partida" -> Retornar· um array com as possiveis de data e horas
	 *
	 * @param string $strTipo
	 * @param string $strData
	 * @return array
	 * @since 1.0
	 */
	public static function formatarData($strTipo, $strData, $separador = "-"){
		
		switch($strTipo)
		{
			
			//Retorna um array com os campos ano, mes, dia, hora, minuto e segundo
			case "partida":
				
				$strData		= ($strData == "") ? "00-00-0000 00:00:00" : $strData;
				$arrDataHora	= explode(" ", $strData); 
				$arrData		= explode("-", $arrDataHora[0]);
				$intDia			= $arrData[2];
				$intMes			= $arrData[1];
				$intAno			= $arrData[0];	
				$intHora		= "00";
				$intMinuto		= "00";
				$intSegundo		= "00";
				if(array_key_exists(1,$arrDataHora))
				{
					
					$arrHora		= explode(":", $arrDataHora[1]);			
					$intHora		= $arrHora[0];
					$intMinuto		= $arrHora[1];
					$intSegundo		= $arrHora[2];
						
				}				
				$array['dia']		= $intDia;
				$array['mes']		= $intMes;
				$array['ano']		= $intAno;
				$array['hora']		= $intHora;
				$array['minuto']	= $intMinuto;
				$array['segundo']	= $intSegundo;
				
				return $array;		
				
			break;
			
			//inverte uma data no formato dd-mm-aaaa hh:ii:ss para o formato aaaa-mm-dd
			case "inverterSemHora":
				$semHora = true;
				
			//inverte uma data no formato dd-mm-aaaa hh:ii:ss para o formato aaaa-mm-dd hh:ii:ss
			case "inverter":
				
				$separador	= ($separador=="") ? "-" : $separador;
				//a data tem que vir como string
				if (!is_string($strData)){
					return false;
				}
				
				//
				$strData = str_replace("/","-",$strData);
				
				$arrDataHora = explode(" ",$strData);
				$arrData = explode("-",$arrDataHora[0]);
				
				//inverter
				$strData = $arrData[2] . $separador . $arrData[1] . $separador . $arrData[0];
				if (!isset($semHora)){
					if (count($arrDataHora) > 1){
						$strData .= " " . $arrDataHora[1];
					}
				}
				
				return $strData;
				
			break;
			
		}
		
	}
	
	/**
	 * M√©todo que ir√° formatar os caracteres da string
	 *
	 * @access public
	 * @static
	 * @param string $string String para formata√ß√£o
	 * @return string
	 */
	public static function formatarCaracteres( $string )
	{
	
		//Definindo os arrays
		$arrComAcento	= array( "Á","É","Í","Ó","Ú","Â","Ê","Î","Ô","Û","Ã","Ñ","Õ","Ä","Ë","Ï","Ö","Ü","À","È","Ì","Ò","Ù","á","é","í","ó","ú","â","ê","î","ô","û","ã","ñ","õ","ä","ë","ï","ö","ü","à","è","ì","ò","ù",".",",",":",";","...","ç","%","?","/","\\","‚Äù","‚Äú","'","!","@","#","$","&","*","(",")","+","=","{","}","[","]","|","<",">","\"","&ordf;","&ordm;","&deg;","‚Äò","‚Äò","&raquo;","-","¬™","¬∫","¬ª","¬¥","√á","‚Äô", " " );
		$arrSemAcento	= array( "a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","","","","","","c","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","c","","" );
	
		//Retornando a string formatada
		return strtolower( str_replace( $arrComAcento, $arrSemAcento, $string ) );
	
	}
	
	/**
	 * M√©todo que ir√° formatar o nome da imagem
	 *
	 * @access public
	 * @static
	 * @param string $strNomeImagem Nome da imagem
	 * @uses self::formatarCaracteres Ir√° formatar os caracteres do nome da imagem
	 * @return string
	 */
	public static function formatarNomeImagem( $strNomeImagem )
	{
		
		//Definindo a extensão do arquivo
		$arrArquivo = explode(".", $strNomeImagem);
		$extensao = $arrArquivo[count($arrArquivo)-1];
	
		//Removendo os caracteres especiais e retornando o nome da imagem
		return md5(time() . '-' . strtolower( str_replace( " ", "-", self::formatarCaracteres( substr( $strNomeImagem, 0, strlen( $strNomeImagem ) - 5 ) ) ) )) . "." . $extensao;
	
	}
	
	/**
	 * MÈtodo que ir· formatar o nome do diretÛrio retirando os acentos, espaÁos e colocando-a em caixa baixa
	 *
	 * @access public
	 * @statuc
	 * @param string $strNome
	 * @return string
	 */
	public static function formatarNome($strString)
	{
		
		//Retirando os acentos
		$strString	= self::retiraAcento($strString);
		
		//Retirando os espaÁos em branco
		$strString	= str_replace(" ","-", strtolower($strString));
		
		//Retornando a string
		return $strString;
		
	}
	
	/**
	 * Método que retira os acentos de uma String e a retorna
	 *
	 * @param String $str
	 * @return String
	 */
	public static function retiraAcento($str)
	{
		$arrAcentos		= array("Á","É","Í","Ó","Ú","Â","Ê","Î","Ô","Û","Ã","Ñ","Õ","Ä","Ë","Ï","Ö","Ü","À","È","Ì","Ò","Ù","á","é","í","ó","ú","â","ê","î","ô","û","ã","ñ","õ","ä","ë","ï","ö","ü","à","è","ì","ò","ù",".",",",":",";","...","ç","%","?","/","\\","","","'","!","@","#","$","*","(",")","+","=","{","}","[","]","|","<",">","\"","&ordf;","&ordm;","&deg;","","","&raquo;","-","ª","º","»","´","~","&","°");
		$arrSemacento	= array("a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","a","n","o","a","e","i","o","u","a","e","i","o","u","","","","","","c","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","e","");
		
		for($intPos = 0 ; $intPos < count($arrAcentos) ; $intPos++){
			$str = str_replace($arrAcentos[$intPos],$arrSemacento[$intPos],$str);
			
		}
		
		return strtolower($str);
	
	}
	
	/**
	 * Método que verifica a permissão do usuário para determinadas operações
	 *
	 * @param String $str
	 * @return String
	 */
	public static function verificarPermissao($modulo, $acao)
	{
	    if(in_array($_SESSION["sesArrOperacaoSistema"][$modulo][$acao], $_SESSION["sesArrOperacaoUsuario"])){
			return true;
		} else {
			return false;
		}
	
	}
	
	/**
	 * Método que retorna o nome da Transação
	 *
	 * @param String $str
	 * @return String
	 */
	public static function nomeTransacao($codigo)
	{
	    
	    switch ($codigo) {
	        case 1:
	            return "Depósito";
	        break;
	        case 2:
	            return "Transferência feita";
	        break;
	        case 3:
	            return "Transferência recebida";
	            break;
	        case 4:
	            return "Saque";
	            break;
	        default:
	            return "";
	        break;
	    }
	    
	}
	
	/**
	 * Método que retorna a situação da Transação
	 *
	 * @param String $str
	 * @return String
	 */
	public static function situacaoTransacao($codigo)
	{
	    
	    switch ($codigo) {
	        case 0:
	            return $review;
	        break;
	        case 1:
	            return $approved;
	        break;
	        case 2:
	            return $rejected;
	        break;
	        default:
	            return "";
	        break;
	    }
	    
	}
	
	/**
	 * Método que retorna o nome da moeda
	 *
	 * @param String $str
	 * @return String
	 */
	public static function nomeMoeda($moeda)
	{
	    
	    switch ($moeda) {
	        case 1:
	            return "BRL";
	            break;
	        case 2:
	            return "USD";
	            break;
	        case 3:
	            return "EUR";
	            break;
	        case 4:
	            return "BTC";
	            break;
	        default:
	            return "";
	        break;
	    }
	    
	}
	
	/**
	 * Método que retorna o símbolo da moeda
	 *
	 * @param String $str
	 * @return String
	 */
	public static function simboloMoeda($moeda)
	{
	    
	    switch ($moeda) {
	        case 1:
	            return "R$";
	            break;
	        case 2:
	            return "US$";
	            break;
	        case 3:
	            return "€";
	            break;
	        case 4:
	            return "₿";
	            break;
	        default:
	            return "";
	            break;
	    }
	    
	}
	
	/**
	 * Método que retorna o valor convertido pela moeda do favorecido
	 *
	 * @param String $str
	 * @return String
	 */
	public static function converterValores($valor, $moeda)
	{
	    
	    $valor = self::formatarValorMoeda($valor, $moeda);
	    
	    require_once(Configuracao::getBaseDir()."modelo/cotacao/ControladorCotacao.php");
	    
	    $objCotacao = ControladorCotacao::buscarUltimoPorMoeda($moeda);
	    
	    $valorFavorecido = $valor*$objCotacao->getValor();
	    
	    return $valorFavorecido;
	    
	}
	
	/**
	 * Método que retorna o valor da taxa para a transação específica
	 *
	 * @param String $str
	 * @return String
	 */
	public static function buscarTaxa($tipoRemetente, $tipoDestinatario, $tipoTransacao)
	{
	    
	    switch ($tipoTransacao) {
	        
	        case 1:
	            switch ($tipoRemetente) {
	                case 1:
	                    $taxa = Taxa::getTaxaDepositoManualPF();
	                    break;
	                case 2:
	                    $taxa = Taxa::getTaxaDepositoManualPJ();
	                    break;
	            }
	            break;
	            
	        case 2:
	            switch ($tipoRemetente) {
	                case 1:
	                    if($tipoDestinatario==1){
	                        $taxa = Taxa::getTaxaTransferenciaInternaPFPF();
	                    }
	                    if($tipoDestinatario==2){
	                        $taxa = Taxa::getTaxaTransferenciaInternaPFPJ();
	                    }
	                    break;
	                case 2:
	                    if($tipoDestinatario==1){
	                        $taxa = Taxa::getTaxaTransferenciaInternaPJPF();
	                    }
	                    if($tipoDestinatario==2){
	                        $taxa = Taxa::getTaxaTransferenciaInternaPJPJ();
	                    }
	                    break;
	            }
	            break;
	            
	        case 4:
	            switch ($tipoRemetente) {
	                case 1:
	                    $taxa = Taxa::getTaxaSaqueManualPF();
	                    break;
	                case 2:
	                    $taxa = Taxa::getTaxaSaqueManualPJ();
	                    break;
	            }
	            break;
	            
	    }
	    
	    return $taxa;
	    
	}
	
	
	
	/**
	 * Método que retorna o valor da transação descontado a taxa
	 *
	 * @param String $str
	 * @return String
	 */
	public static function calcularTaxa($valor, $taxa, $tipo, $moeda)
	{
	    
	    $valor = self::converterValores($valor, $moeda);
	    
	    switch ($tipo) {
	        
	        case 1:
	        case 2:	                
	        case 4:
	            $taxa = $valor*$taxa;
	            $valorFavorecido = $valor - $taxa;
	            break;
	        default:
	            $taxa = 0;
	            $valorFavorecido = $valor;
	    }
	    
	    return array($valorFavorecido, $taxa);
	    
	}
	
	/**
	 * Método que retorna o valor monetário formatado para banco de dados
	 *
	 * @param String $str
	 * @return String
	 */
	public static function formatarValorMoeda($valor, $moeda)
	{
	    
	    switch ($moeda) {
	        case 1:
	        case 3:
	            $valor = str_replace(",", ".", str_replace(".", "", $valor));
	           break;
	        case 2:
	        case 4:
	            $valor = str_replace(",", "", $valor);
	            break;
	    }
	        
	    return $valor;
	    
	}
	
	/**
	 * Método que retorna o valor monetário formatado para banco de dados
	 *
	 * @param String $str
	 * @return String
	 */
	public static function formatarValorRetorno($valor, $moeda)
	{
	    
	    switch ($moeda) {
	        case 1:
	        case 3:
	            $valor = self::simboloMoeda($moeda). " " . number_format($valor, 2, ',', '.');
	            break;
	        case 2:
	        case 4:
	            $valor = self::simboloMoeda($moeda). " " . number_format($valor, 2, '.', ',');
	            break;
	    }
	    
	    return $valor;
	    
	}

	public static function verIdioma()
	{
		if (!isset($_COOKIE["lang"])) {
			$expire=time()+60*60*24*30;
			// setcookie("lang", "en", "");
			setcookie("lang", "en", $expire, "/");
			return Configuracao::getLangEn();
		}
		elseif ($_COOKIE["lang"] == "en") {
			return Configuracao::getLangEn();
		}
		elseif ($_COOKIE["lang"] == "pt") {
			return Configuracao::getLangPt();
		}
		elseif ($_COOKIE["lang"] == "es") {
			return Configuracao::getLangEs();
		}
	}
	
	public static function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos){
	    $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
	    $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
	    $nu = "012345678901234567890123456789"; // $nu contem os números
	    $si = "!@#$%¨&*()_+="; // $si contem os símbolos
	    
	    $senha = "";
	    
	    if ($maiusculas){
	        $senha .= str_shuffle($ma);
	    }
	    
	    if ($minusculas){
	        $senha .= str_shuffle($mi);
	    }
	    
	    if ($numeros){
	        $senha .= str_shuffle($nu);
	    }
	    
	    if ($simbolos){
	        $senha .= str_shuffle($si);
	    }
	    
	    return substr(str_shuffle($senha),0,$tamanho);
	}
	
	public static function isMobile($userAgent) {
	    $mobile = FALSE;
	    
	    $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");
	    
	    foreach($user_agents as $user_agent){
	        
	        if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
	            $mobile = TRUE;
	            
	            $modelo = $user_agent;
	            
	            break;
	        }
	    }
	    
	    return $mobile;
	}
	
	public static function reduzirNome($nome) {
	    
	    $usuNome = $nome; 
	    $nomeVelho = explode(" ",$usuNome);
	    
	    if ($nomeVelho[0] == end($nomeVelho)){
	        
	        return $nomeVelho[0];
	    }
	    if ($nomeVelho[0] != end($nomeVelho)){
	        
    	    $nomeNovo = $nomeVelho[0]." ".end($nomeVelho);
    	    
    	    return $nomeNovo;
    	}
      
      }

    function compararStringsASCII($a, $b) {
        $at = iconv('UTF-8', 'ASCII//TRANSLIT', $a);
        $bt = iconv('UTF-8', 'ASCII//TRANSLIT', $b);
        return strcmp($at, $bt);
    }

}


?>